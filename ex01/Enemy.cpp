/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:58:42 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 15:08:50 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "Enemy.hpp"

Enemy::Enemy() {}
Enemy::~Enemy() {}

Enemy::Enemy(const Enemy& obj) {
	*this = obj;
}

Enemy& Enemy::operator=(const Enemy& obj) {
	this->_hp = obj._hp;
	this->_type = obj._type;
	return (*this);
}

Enemy::Enemy(int hp, std::string const &type) : _hp(hp), _type(type) {}

void				Enemy::takeDamage(int damage) {
	if (damage < 0)
		return ;
	this->_hp -= damage;
}

std::string			Enemy::getType(void) const {
	return (this->_type);
}

int					Enemy::getHP(void) const {
	return (this->_hp);
}
