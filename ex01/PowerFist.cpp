/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:59:15 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 13:56:27 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PowerFist.hpp"

PowerFist::PowerFist() : AWeapon ("Power Fist", 8, 50) {}
PowerFist::~PowerFist() {}

PowerFist::PowerFist(PowerFist const &obj) {
	*this = obj;
}

void				PowerFist::attack(void) const {
	std::cout << "* pschhh... SBAM! *" << std::endl;
}

PowerFist			&PowerFist::operator=(PowerFist const &obj) {
	AWeapon::operator=(obj);
	return (*this);
}
