/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:59:28 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 15:09:29 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperMutant.hpp"

SuperMutant::SuperMutant() : Enemy(170, "Super Mutant") {
	std::cout << "Gaaah. Me want smash heads !" << std::endl;
}

SuperMutant::~SuperMutant() {
	std::cout << "Aaargh ..." << std::endl;
}

SuperMutant::SuperMutant(SuperMutant const &obj) {
	*this = obj;
}

SuperMutant			&SuperMutant::operator=(SuperMutant const &obj) {
	Enemy::operator=(obj);
	return (*this);
}

void			SuperMutant::takeDamage(int damage) {
	Enemy::takeDamage(damage - 3);
}
