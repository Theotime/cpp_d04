/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:58:55 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 13:55:10 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"

AWeapon::AWeapon() {}
AWeapon::~AWeapon() {}

AWeapon::AWeapon(AWeapon const &obj) {
	*this = obj;
}

AWeapon::AWeapon(std::string const &name, int apcost, int damage) : _damage(damage), _ap_cost(apcost), _name(name) {}

AWeapon				&AWeapon::operator=(AWeapon const &obj) {
	this->_damage = obj._damage;
	this->_ap_cost = obj._damage;
	this->_name = obj._name;
	return (*this);
}

int					AWeapon::getDamage(void) const {
	return (this->_damage);
}

int					AWeapon::getAPCost(void) const {
	return (this->_ap_cost);
}

std::string			AWeapon::getName(void) const {
	return (this->_name);
}
