/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:58:57 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 13:50:40 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __AWEAPON_HPP__
# define __AWEAPON_HPP__

#include <iostream>

class AWeapon {

	public:
		AWeapon(AWeapon const &obj);
		AWeapon(std::string const &name, int apcost, int damage);
		virtual ~AWeapon();

		virtual void		attack() const = 0;

		int					getDamage(void) const;
		int					getAPCost(void) const;
		std::string			getName(void) const;

		AWeapon				&operator=(AWeapon const & obj);

	protected:
		AWeapon();

	private:
		int				_damage;
		int				_ap_cost;

		std::string		_name;

};

#endif
