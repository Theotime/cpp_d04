/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:58:38 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 14:55:37 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef __ENEMY_HPP__
# define __ENEMY_HPP__

#include <iostream>

class Enemy
{
	public:
		Enemy(int hp, std::string const &type);
		Enemy(const Enemy& obj);
		virtual ~Enemy();
		Enemy			&operator=(const Enemy &obj);

		virtual void		takeDamage(int);

		std::string			getType(void) const ;
		int					getHP(void) const;

	protected:
		Enemy();

		int				_hp;
		std::string		_type;
};
#endif
