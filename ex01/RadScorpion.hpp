/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:59:24 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 15:48:22 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __RADSCORPION_HPP__
# define __RADSCORPION_HPP__

# include "Enemy.hpp"

class RadScorpion : public virtual Enemy {

	public:
		RadScorpion();
		RadScorpion(RadScorpion const &obj);
		virtual ~RadScorpion();

		RadScorpion			&operator=(RadScorpion const &obj);
	private:

};

#endif
