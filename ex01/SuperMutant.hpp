/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:59:33 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 15:03:57 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __SUPERMUTANT_HPP__
# define __SUPERMUTANT_HPP__

# include "Enemy.hpp"

class SuperMutant : virtual public Enemy {

	public:
		SuperMutant(SuperMutant const &obj);
		virtual ~SuperMutant();

		SuperMutant			&operator=(SuperMutant const &obj);

		virtual void		takeDamage(int damage);

	private:
		SuperMutant();
};

#endif
