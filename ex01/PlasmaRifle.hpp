/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:59:12 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 13:49:56 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __PLASMARIFLE_HPP__
# define __PLASMARIFLE_HPP__

#include "AWeapon.hpp"

class PlasmaRifle : virtual public AWeapon {

	public:
		PlasmaRifle();
		PlasmaRifle(PlasmaRifle const &obj);
		virtual ~PlasmaRifle();

		PlasmaRifle			&operator=(PlasmaRifle const &obj);

		void				attack(void) const;

	private:
};

#endif
