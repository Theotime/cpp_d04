/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:58:26 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 16:57:48 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character() : _weapon(NULL) {}
Character::~Character() {}

Character::Character(Character const &obj) : _weapon(NULL){
	*this = obj;
}

Character			&Character::operator=(Character const &obj) {
	(void)obj;
	return (*this);
}

std::ostream			&operator<<(std::ostream &stream, Character const &character) {
	AWeapon			*weapon;

	weapon = character.getWeapon();
	stream << character.getName() << " has " << character.getAP() << " AP and ";
	if (weapon)
		stream << "wields a " << weapon->getName();
	else
		stream << "is unarmed";
	stream << std::endl;
	return (stream);
}


Character::Character(std::string const &name) : _name(name), _ap(40), _max_ap(40), _weapon(NULL) {}

std::string				Character::getName(void) const {
	return (this->_name);
}

AWeapon					*Character::getWeapon(void) const  {
	return (this->_weapon);
}

int						Character::getAP(void) const {
	return (this->_ap);
}

void					Character::equip(AWeapon *weapon) {
	this->_weapon = weapon;
}

void					Character::attack(Enemy *enemy) {
	AWeapon		*weapon = this->getWeapon();

	if (!weapon || this->_ap <= 0)
		return ;
	enemy->takeDamage(weapon->getDamage());
	this->consumeAP(weapon->getAPCost());
	std::cout << this->getName() << " attacks " << enemy->getType() << " with a " << weapon->getName() << std::endl;
	weapon->attack();
	if (enemy->getHP() <= 0)
		delete (enemy);
}

void					Character::recoverAP(void) {
	this->_ap += 10;
	if (this->_ap >= this->_max_ap)
		this->_ap = this->_max_ap;
}

void					Character::consumeAP(int ap) {
	if (ap <= 0)
		return ;
	this->_ap -= ap;
	if (this->_ap < 0)
		this->_ap = 0;
}
