/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:59:18 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 13:51:15 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __POWERFIST_HPP__
# define __POWERFIST_HPP__

#include "AWeapon.hpp"

class PowerFist : virtual public AWeapon {

	public:
		PowerFist();
		PowerFist(PowerFist const &obj);
		virtual ~PowerFist();

		void				attack(void) const;

		PowerFist			&operator=(PowerFist const &obj);

	private:

};

#endif
