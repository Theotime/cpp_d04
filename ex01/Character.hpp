/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:57:33 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 17:13:09 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __CHARACTER_HPP__
# define __CHARACTER_HPP__

# include <iostream>
# include "AWeapon.hpp"
# include "Enemy.hpp"

class Character {

	public:
		Character(Character const &obj);
		~Character();
		Character			&operator=(Character const &obj);

		Character(std::string const &name);

		void			recoverAP(void);
		void			equip(AWeapon *weapon);
		void			attack(Enemy *enemy);
		void			consumeAP(int ap);

		std::string		getName(void) const;
		AWeapon			*getWeapon(void) const;
		int				getAP(void) const;

	protected:
		Character();

		std::string			_name;
		int					_ap;
		int					_max_ap;
		AWeapon				*_weapon;
};

std::ostream			&operator<<(std::ostream &stream, Character const &character);

#endif
