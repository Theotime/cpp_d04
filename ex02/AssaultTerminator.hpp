#ifndef __ASSAULTTERMINATOR_HPP__
# define __ASSAULTTERMINATOR_HPP__

#include "ISpaceMarine.hpp"
#include <iostream>

class AssaultTerminator : public ISpaceMarine {

	public:
		AssaultTerminator();
		AssaultTerminator(AssaultTerminator const &obj);
		~AssaultTerminator();

		AssaultTerminator			&operator=(AssaultTerminator const &obj);

		virtual ISpaceMarine		*clone(void) const;
		virtual void				battleCry(void) const;
		virtual void				rangedAttack(void) const;
		virtual void				meleeAttack(void) const;

	protected:

	private:

};

#endif