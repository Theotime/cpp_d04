#ifndef __ISQUAD_HPP__
# define __ISQUAD_HPP__

#include "ISpaceMarine.hpp"

class ISquad {

	public:
		virtual ~ISquad() {};
		virtual int					getCount(void) const = 0;
		virtual ISpaceMarine		*getUnit(int n) const = 0;
		virtual int					push(ISpaceMarine *spaceMarine) = 0;
};

#endif
