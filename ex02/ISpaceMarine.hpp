#ifndef __ISPACEMARINE_HPP__
# define __ISPACEMARINE_HPP__

class ISpaceMarine {

	public:
		virtual ~ISpaceMarine() {}
		virtual ISpaceMarine		*clone(void) const = 0;
		virtual void				battleCry(void) const = 0;
		virtual void				rangedAttack(void) const = 0;
		virtual void				meleeAttack(void) const = 0;

};

#endif