#include "Squad.hpp"

Squad::Squad() : _count(0) {}
Squad::Squad(Squad const &obj) {
	*this = obj;
}

Squad::~Squad() {
	size_t		len = this->getCount();

	for (size_t i = 0; i < len; ++i) {
		delete this->_units[i];
	}
}

Squad			&Squad::operator=(Squad const &obj) {
	this->_count = obj._count;
	this->_units = obj._units;
	return (*this);
}

int				Squad::getCount(void) const {
	return (this->_count);
}

int				Squad::push(ISpaceMarine *spaceMarine) {
	ISpaceMarine		**spaceMarines;

	if (spaceMarine == NULL)
		return (this->getCount());
	++this->_count;
	spaceMarines = new ISpaceMarine*[this->_count];
	for (int i = 0; i < this->_count - 1; ++i) 
		spaceMarines[i] = this->_units[i]->clone();
	spaceMarines[this->_count - 1] = spaceMarine;
	this->_units = spaceMarines;
	return (this->getCount());
}

ISpaceMarine	*Squad::getUnit(int n) const {
	if (n < 0 || n >= this->_count)
		return (NULL);
	return (this->_units[n]);
}
