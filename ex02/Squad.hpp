#ifndef __SQUAD_HPP__
# define __SQUAD_HPP__

# include "ISquad.hpp"
# include <stddef.h>

class Squad : public ISquad {

	public:
		Squad();
		Squad(Squad const &obj);
		virtual ~Squad();
		Squad					&operator=(Squad const &obj);

		virtual int				getCount(void) const;
		virtual ISpaceMarine	*getUnit(int n) const;
		virtual int				push(ISpaceMarine *spaceMarine);

	private:
		int				_count;
		ISpaceMarine	**_units;

};

#endif
