#ifndef __TACTICALMARINE_HPP__
# define __TACTICALMARINE_HPP__

#include "ISpaceMarine.hpp"
#include <iostream>

class TacticalMarine : public ISpaceMarine {

	public:
		TacticalMarine();
		TacticalMarine(TacticalMarine const &obj);
		virtual ~TacticalMarine();

		TacticalMarine			&operator=(TacticalMarine const &obj);

		virtual ISpaceMarine		*clone(void) const;
		virtual void				battleCry(void) const;
		virtual void				rangedAttack(void) const;
		virtual void				meleeAttack(void) const;

	protected:

	private:

};

#endif