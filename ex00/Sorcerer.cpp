/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/10 00:02:31 by triviere          #+#    #+#             */
/*   Updated: 2015/01/10 06:40:36 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"

Sorcerer::Sorcerer():
	_name("Non connu"),
	_title("Aucun")
{
	this->born();
}

Sorcerer::Sorcerer(Sorcerer const &obj) {
	*this = obj;
	this->born();
}

Sorcerer::Sorcerer(std::string name, std::string title):
	_name(name),
	_title(title)
{
	this->born();
}


Sorcerer::~Sorcerer() {
	this->die();
}

std::string			Sorcerer::getName()			const { return this->_name;			}
std::string			Sorcerer::getTitle()		const { return this->_title;		}

void				Sorcerer::born() {
	std::cout << this->_name << ", "<< this->_title << ", is born !" << std::endl;
}

void				Sorcerer::die() {
	std::cout << this->_name << ", " << this->_title << ", is dead. Consequences will never be the same !" << std::endl;
}

void				Sorcerer::polymorph(Victim const &victim) const {
	victim.getPolymorphed();
}

Sorcerer			&Sorcerer::operator=(Sorcerer const &o) {
	Sorcerer		tmp(o);

	*this = tmp;
	return (*this);
}

std::ostream		&operator<<(std::ostream &str, Sorcerer const &o) {
	str << "I am " << o.getName() << ", " << o.getTitle() << ", and I like ponies !" << std::endl;
	return (str);
}
