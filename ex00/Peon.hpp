/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/10 00:02:25 by triviere          #+#    #+#             */
/*   Updated: 2015/01/10 06:47:21 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __PEON_HPP__
# define __PEON_HPP__

#include "Victim.hpp"
#include <string>
#include <iostream>

class Peon: public Victim {
	public:
		Peon();
		Peon(std::string const &name);
		Peon(Peon const &obj);
		~Peon();

		void		born();
		void		die();

		void		getPolymorphed() const;

		Peon		&operator=(Peon const &o);
	private:
		
};

#endif
