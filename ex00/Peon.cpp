/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/10 00:02:20 by triviere          #+#    #+#             */
/*   Updated: 2016/01/08 12:51:24 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Peon.hpp"

Peon::Peon() : Victim("Peon") {
	this->born();
}

Peon::Peon(std::string const &name): Victim(name) {
	this->born();
}

Peon::Peon(Peon const &obj): Victim(obj.getName()) {}

Peon::~Peon() {
	this->die();
}

Peon		&Peon::operator=(Peon const &o) {
	Peon		tmp(o);

	*this = tmp;
	return (*this);
}

void		Peon::getPolymorphed() const {
	std::cout << this->_name << " has been turned into a pink pony !" << std::endl;
}

void		Peon::born() {
	std::cout << "Zog zog." << std::endl;
}

void		Peon::die() {
	std::cout << "Bleuark..." << std::endl;
}
