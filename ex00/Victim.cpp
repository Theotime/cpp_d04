/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/10 00:02:43 by triviere          #+#    #+#             */
/*   Updated: 2015/01/10 06:40:18 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.hpp"

Victim::Victim():
	_name("John doe")
{
	this->born();
}

Victim::Victim(std::string name):
	_name(name)
{
	this->born();
}

Victim::~Victim() {
	this->die();
}

Victim::Victim(Victim const &obj) {
	*this = obj;
}

std::string			Victim::getName()						const { return this->_name; }

void				Victim::born() {
	std::cout << "Some random victim called " << this->_name << " just popped !" << std::endl;
}

void				Victim::die() {
	std::cout << "Victim " << this->_name << " just died for no apparent reason !" << std::endl;
}

void				Victim::getPolymorphed()				const {
	std::cout << this->_name << " has been turned into a cute little sheep !" << std::endl;
}

Victim				&Victim::operator=(Victim const &o) {
	Victim		tmp(o);

	*this = tmp;
	return (*this);
}

std::ostream		&operator<<(std::ostream &str, Victim const &o) {
	str << "I'm " << o.getName() << " and i like otters !" << std::endl;
	return (str);
}
