/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/10 00:02:50 by triviere          #+#    #+#             */
/*   Updated: 2015/01/10 06:31:19 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __VICTIM_HPP__
# define __VICTIM_HPP__

# include <string>
# include <iostream>

class Victim {
	public:
		Victim();
		Victim(std::string name);
		Victim(Victim const &obj);
		~Victim();

		std::string		getName()				const;

		void			born();
		void			die();
		virtual void	getPolymorphed()		const;

		Victim			&operator=(Victim const &obj);

	protected:
		std::string		_name;
};

std::ostream		&operator<<(std::ostream &str, Victim const &o);

#endif
