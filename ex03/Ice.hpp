#ifndef __ICE_HPP__
# define __ICE_HPP__

# include "AMateria.hpp"

class Ice : public AMateria {

	public:
		Ice();
		Ice(Ice const &obj);
		virtual ~Ice();
		virtual Ice				&operator=(Ice const &obj);

		virtual Ice				*clone() const;
		virtual void			use(ICharacter &target) {
			AMateria::use(target);
			std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
		}

	private:

};

#endif