/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 19:34:56 by triviere          #+#    #+#             */
/*   Updated: 2016/01/09 19:34:57 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __AMATERIA_HPP__
# define __AMATERIA_HPP__

# include "ICharacter.hpp"
# include <iostream>

class ICharacter;
class AMateria {

	public:
		AMateria();
		AMateria(AMateria const &obj);
		AMateria(std::string const & type);
		virtual ~AMateria();
		std::string const		&getType() const;
		unsigned int			getXP() const;
		virtual AMateria		*clone() const = 0;
		virtual void			use(ICharacter &target);

		virtual AMateria		&operator=(AMateria const &AMateria);

	private:
		unsigned int	_xp;
		std::string		_type;

};

#endif
