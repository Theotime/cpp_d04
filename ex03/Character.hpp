/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 19:34:50 by triviere          #+#    #+#             */
/*   Updated: 2016/01/09 19:34:51 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __CHARACTER_HPP__
# define __CHARACTER_HPP__

# include "ICharacter.hpp"

class Character : public ICharacter {

	public:
		Character();
		Character(std::string const &name);
		Character(Character const &obj);
		virtual ~Character();
		Character					&operator=(Character const &obj);

		virtual std::string const	&getName(void) const;
		virtual void				equip(AMateria* m);
		virtual void				unequip(int idx);
		virtual void				use(int idx, ICharacter& target);

	private:
		std::string		_name;
		AMateria		*_equipment[4];
		int				_nbEquipment;

};

#endif
