/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 19:34:41 by triviere          #+#    #+#             */
/*   Updated: 2016/01/09 19:37:37 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"

Character::Character() : _nbEquipment(0) {
	for (size_t i = 0; i < 4; ++i)
		this->_equipment[i] = NULL;
}

Character::Character(std::string const &name) : _name(name), _nbEquipment(0) {}

Character::Character(Character const &obj) {
	this->_name = obj._name;
	this->_nbEquipment = obj._nbEquipment;
	for (size_t i = 0; i < 4; ++i) {
		this->_equipment[i] = obj._equipment[i];
	}
	*this = obj;
}

Character::~Character() {
	for (size_t i = 0; i < 4; ++i) {
		delete this->_equipment[i];
		this->_equipment[i] = NULL;
	}
}

Character			&Character::operator=(Character const &obj) {
	this->_name = obj._name;
	this->_nbEquipment = obj._nbEquipment;
	for (size_t i = 0; i < 4; ++i) {
		this->_equipment[i] = obj._equipment[i];
	}
	return (*this);
}

std::string const	&Character::getName(void) const {
	return (this->_name);
}

void				Character::equip(AMateria* m) {
	if (this->_nbEquipment >= 4)
		return ;
	for (size_t i = 0; i < 4; ++i) {
		if (this->_equipment[i] == NULL) {
			this->_equipment[i] = m;
			this->_nbEquipment++;
			break ;	
		}
	}
}

void				Character::unequip(int idx) {
	if (this->_equipment[idx]) {
		this->_equipment[idx] = NULL;
		--this->_nbEquipment;
	}
}

void				Character::use(int idx, ICharacter& target) {
	if (!this->_equipment[idx])
		return ;
	this->_equipment[idx]->use(target);
}
