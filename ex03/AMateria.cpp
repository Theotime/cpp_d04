/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 19:35:00 by triviere          #+#    #+#             */
/*   Updated: 2016/01/09 19:35:00 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMateria.hpp"

AMateria::AMateria() : _xp(0) {}
AMateria::AMateria(AMateria const &obj) {
	*this = obj;
}

AMateria::~AMateria() {}

AMateria			&AMateria::operator=(AMateria const &obj) {
	this->_xp = obj._xp;
	this->_type = obj._type;
	return (*this);
}

AMateria::AMateria(std::string const &type) : _xp(0), _type(type)  {}

void				AMateria::use(ICharacter &target) {
	(void)target;
	this->_xp += 10;
}

unsigned int		AMateria::getXP(void) const {
	return (this->_xp);
}

std::string const		&AMateria::getType(void) const {
	return (this->_type);
}
