/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 19:34:45 by triviere          #+#    #+#             */
/*   Updated: 2016/01/09 19:34:46 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cure.hpp"

Cure::Cure() : AMateria("cure") {}
Cure::Cure(Cure const &obj) {
	*this = obj;
}
Cure::~Cure() {}

Cure			&Cure::operator=(Cure const &obj) {
	AMateria::operator=(obj);
	return (*this);
}

Cure			*Cure::clone(void) const {
	return (new Cure(*this));
}
