#include "Ice.hpp"

Ice::Ice() : AMateria("ice") {}
Ice::Ice(Ice const &obj) {
	*this = obj;
}
Ice::~Ice() {}

Ice			&Ice::operator=(Ice const &obj) {
	AMateria::operator=(obj);
	return (*this);
}

Ice			*Ice::clone(void) const {
	return (new Ice(*this));
}