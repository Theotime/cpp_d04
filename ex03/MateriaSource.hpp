/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 19:35:20 by triviere          #+#    #+#             */
/*   Updated: 2016/01/09 19:35:21 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __MATERIASOURCE_HPP__
# define __MATERIASOURCE_HPP__

# include "AMateria.hpp"
# include "IMateriaSource.hpp"
# include <iostream>

class MateriaSource : public IMateriaSource {

	public:
		MateriaSource();
		MateriaSource(MateriaSource const &obj);
		virtual ~MateriaSource() {}

		MateriaSource		&operator=(MateriaSource const &obj);

		virtual void		learnMateria(AMateria*);
		virtual AMateria	*createMateria(std::string const & type);

	private:
		AMateria			*_materia[4];
		size_t				_nbMateria;
};

#endif
