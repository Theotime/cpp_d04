/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 19:35:17 by triviere          #+#    #+#             */
/*   Updated: 2016/01/09 19:39:24 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.hpp"

MateriaSource::MateriaSource() : _nbMateria(0) {
	for (size_t i = 0; i < 4; ++i) {
		this->_materia[i] = NULL;
	}
}

MateriaSource::MateriaSource(MateriaSource const &obj) {
	*this = obj;
}

MateriaSource		&MateriaSource::operator=(MateriaSource const &obj) {
	for (size_t i = 0; i < 4; ++i) {
		this->_materia[i] = obj._materia[i];
	}
	this->_nbMateria = obj._nbMateria;
	return (*this);
}

void				MateriaSource::learnMateria(AMateria *materia) {
	for (size_t i = 0; i < 4; ++i) {
		if (this->_materia[i] == NULL) {
			this->_materia[i] = materia;
			break ;
		}
	}
}

AMateria			*MateriaSource::createMateria(std::string const &type) {
	for (size_t i = 0; i < 4; ++i) {
		if (this->_materia[i] != NULL && this->_materia[i]->getType() == type) {
			return (this->_materia[i]);
		}
	}
	return (NULL);
}
