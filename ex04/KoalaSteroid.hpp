#ifndef __KOALASTEROID_HPP__
# define __KOALASTEROID_HPP__

# include "IAsteroid.hpp"
# include "StripMiner.hpp"
# include "DeepCoreMiner.hpp"
# include <iostream>

class BocalSteroid : public IAsteroid {

	public:
		BocalSteroid();
		BocalSteroid(std::string const &name);
		BocalSteroid(BocalSteroid const &obj);
		~BocalSteroid();
		BocalSteroid		&operator=(BocalSteroid const &obj);

		virtual std::string		getName(void) const;
		virtual std::string		beMined(StripMiner *laser) const;
		virtual std::string		beMined(DeepCoreMiner *laser) const;

	protected:
		std::string				_name;
};

#endif