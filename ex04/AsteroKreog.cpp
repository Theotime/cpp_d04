#include "AsteroKreog.hpp"

AsteroBocal::AsteroBocal() {}
AsteroBocal::~AsteroBocal() {}

AsteroBocal::AsteroBocal(std::string const &name) : _name(name) {}

AsteroBocal::AsteroBocal(AsteroBocal const &obj) {
	*this = obj;
}

std::string		AsteroBocal::getName(void) const {
	return this->_name;
}

AsteroBocal			&AsteroBocal::operator=(AsteroBocal const &obj) {
	(void)obj;
	return (*this);
}

std::string			AsteroBocal::beMined(StripMiner *laser) const {
	(void)laser;
	return "Flavium";
}

std::string			AsteroBocal::beMined(DeepCoreMiner *laser) const {
	(void)laser;
	return "Thorite";
}