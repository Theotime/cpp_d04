#ifndef __MININGBARGE_HPP__
# define __MININGBARGE_HPP__

# include "IMiningLaser.hpp"

class MiningBarge {

	public:
		MiningBarge();
		MiningBarge(MiningBarge const &obj);
		~MiningBarge();

		MiningBarge			&operator=(MiningBarge const &obj);

		void				equip(IMiningLaser *laser);
		void				mine(IAsteroid*) const;

	private:
		IMiningLaser		*_lasers[4];
		int					_nbLasers;
};

#endif