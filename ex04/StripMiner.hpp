#ifndef __STRIPMINER_HPP__
# define __STRIPMINER_HPP__

# include "IAsteroid.hpp"
# include "IMiningLaser.hpp"

class StripMiner : public IMiningLaser {

	public:
		StripMiner();
		StripMiner(StripMiner const &obj);
		virtual ~StripMiner();

		StripMiner			&operator=(StripMiner const &obj);

		virtual void				mine(IAsteroid *asteroid);

};

#endif