#include "MiningBarge.hpp"

MiningBarge::MiningBarge() : _nbLasers(0) {
	for (size_t i = 0; i < 4; ++i) {
		this->_lasers[i] = 0;
	}
}
MiningBarge::~MiningBarge() {}

MiningBarge::MiningBarge(MiningBarge const &obj) {
	*this = obj;
}

MiningBarge			&MiningBarge::operator=(MiningBarge const &obj) {
	for (size_t i = 0; i < 4; ++i)
		this->_lasers[i] = obj._lasers[i];
	this->_nbLasers = obj._nbLasers;
	return (*this);
}

void				MiningBarge::equip(IMiningLaser *laser) {
	if (this->_nbLasers >= 4)
		return ;
	this->_lasers[this->_nbLasers++] = laser;
}

void				MiningBarge::mine(IAsteroid *as) const {
	for (size_t i = 0; i < 4; ++i) {
		if (this->_lasers[i] != 0)
			this->_lasers[i]->mine(as);
	}
}