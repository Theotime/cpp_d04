#ifndef __IASTEROID_HPP__
# define __IASTEROID_HPP__

# include <iostream>

class DeepCoreMiner;
class StripMiner;

class IAsteroid {

	public:
		virtual ~IAsteroid() {}
		virtual std::string		getName(void) const = 0;
		virtual std::string		beMined(StripMiner *laser) const = 0;
		virtual std::string		beMined(DeepCoreMiner *laser) const = 0;

};

#endif