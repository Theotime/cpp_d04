#include "DeepCoreMiner.hpp"

DeepCoreMiner::DeepCoreMiner() {}
DeepCoreMiner::~DeepCoreMiner() {}

DeepCoreMiner::DeepCoreMiner(DeepCoreMiner const &obj) {
	*this = obj;
}

DeepCoreMiner			&DeepCoreMiner::operator=(DeepCoreMiner const &obj) {
	(void)obj;
	return (*this);
}

void				DeepCoreMiner::mine(IAsteroid *asteroid) {
	std::cout << "* mining deep ... got " << asteroid->beMined(this) << " ! *" << std::endl;
}