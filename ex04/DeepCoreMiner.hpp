#ifndef __DEEPCOREMINER_HPP__
# define __DEEPCOREMINER_HPP__

# include "IAsteroid.hpp"
# include "IMiningLaser.hpp"

class DeepCoreMiner : public IMiningLaser {

	public:
		DeepCoreMiner();
		DeepCoreMiner(DeepCoreMiner const &obj);
		virtual ~DeepCoreMiner();

		DeepCoreMiner			&operator=(DeepCoreMiner const &obj);

		virtual void				mine(IAsteroid *asteroid);

};

#endif