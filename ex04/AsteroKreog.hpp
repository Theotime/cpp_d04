#ifndef __ASTEROKREOG_HPP__
# define __ASTEROKREOG_HPP__

# include "IAsteroid.hpp"
# include "StripMiner.hpp"
# include "DeepCoreMiner.hpp"
# include <iostream>

class AsteroBocal : public IAsteroid {

	public:
		AsteroBocal();
		AsteroBocal(std::string const &name);
		AsteroBocal(AsteroBocal const &obj);
		~AsteroBocal();
		AsteroBocal			&operator=(AsteroBocal const &obj);

		virtual std::string		getName(void) const;

		virtual std::string		beMined(StripMiner *laser) const;
		virtual std::string		beMined(DeepCoreMiner *laser) const;

	protected:
		std::string				_name;

};

#endif