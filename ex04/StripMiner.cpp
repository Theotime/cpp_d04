#include "StripMiner.hpp"

StripMiner::StripMiner() {}
StripMiner::~StripMiner() {}

StripMiner::StripMiner(StripMiner const &obj) {
	*this = obj;
}

StripMiner			&StripMiner::operator=(StripMiner const &obj) {
	(void)obj;
	return (*this);
}

void				StripMiner::mine(IAsteroid *asteroid) {
	std::cout << "* strip mining ... got " << asteroid->beMined(this) << " ! *" << std::endl;
}