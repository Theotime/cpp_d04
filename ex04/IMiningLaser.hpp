#ifndef __IMININGLASER_HPP__
# define __IMININGLASER_HPP__

# include <iostream>
// # include "IAsteroid.hpp"
class IAsteroid;

class IMiningLaser {

	public:
		virtual ~IMiningLaser() {}
		virtual void				mine(IAsteroid *asteroid) = 0;

};

#endif