#include "KoalaSteroid.hpp"

BocalSteroid::BocalSteroid() {}
BocalSteroid::BocalSteroid(std::string const &name) : _name(name) {}
BocalSteroid::~BocalSteroid() {}

BocalSteroid::BocalSteroid(BocalSteroid const &obj) {
	*this = obj;
}

std::string		BocalSteroid::getName(void) const {
	return this->_name;
}

BocalSteroid		&BocalSteroid::operator=(BocalSteroid const &obj) {
	(void)obj;
	return (*this);
}

std::string			BocalSteroid::beMined(StripMiner *laser) const {
	(void)laser;
	return "Krpite";
}

std::string			BocalSteroid::beMined(DeepCoreMiner *laser) const {
	(void)laser;
	return "Zazium";
}